-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 06, 2022 at 11:45 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci_uas`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `harga` int(11) NOT NULL,
  `gambar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id`, `nama`, `harga`, `gambar`) VALUES
(3, 'Kursi Kulit', 199000, 'Group 3742 (2).png'),
(4, 'Kursi Santai', 99000, 'Group 3742.png'),
(5, 'Kursi Aesthetic', 199000, 'Group 3742 (1).png'),
(7, 'Kursi Santai 002', 699000, 'Group 3742 (3).png'),
(8, 'Kursi Sofa', 499000, 'kursi (4).png'),
(9, 'Kursi Kayu', 169000, 'kursi (1).png'),
(10, 'Kursi Sofa 002', 299000, 'kursi (2).png'),
(11, 'Kursi Aestethic 002', 259000, 'kursi (3).png');

-- --------------------------------------------------------

--
-- Table structure for table `cekout`
--

CREATE TABLE `cekout` (
  `id_ck` int(11) NOT NULL,
  `nama_ck` varchar(255) NOT NULL,
  `harga_ck` int(11) NOT NULL,
  `gambar_ck` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cekout`
--

INSERT INTO `cekout` (`id_ck`, `nama_ck`, `harga_ck`, `gambar_ck`) VALUES
(12, 'Kursi Santai', 99000, 'Group 3742.png'),
(15, 'Kursi Kulit', 199000, 'Group 3742 (2).png'),
(16, 'Kursi Kulit', 199000, 'Group 3742 (2).png');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `version` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `group` varchar(255) NOT NULL,
  `namespace` varchar(255) NOT NULL,
  `time` int(11) NOT NULL,
  `batch` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `version`, `class`, `group`, `namespace`, `time`, `batch`) VALUES
(1, '2022-07-03-135621', 'App\\Database\\Migrations\\Users', 'default', 'App', 1656857231, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `password`, `created_at`, `updated_at`) VALUES
('reza', '$2y$10$ZoK5g10zLcTXovRhkPl5huyi11VWTBMG/AYFYiCilF47vESGA5I/G', '2022-07-05 23:32:28', '2022-07-05 23:32:28'),
('tioanjay', '$2y$10$gMOIImsA3VDACJ8zASi09.CKX25AxOw7zqtkEPr/JKTxXyP9qcV9K', '2022-07-03 22:56:24', '2022-07-03 22:56:24'),
('wicaksono', '$2y$10$fIqjSoInixxZ.Mls5dzjLeG39sMUr.dGW5yoi7QZQmUIYCG2/Fiem', '2022-07-05 23:44:09', '2022-07-05 23:44:09'),
('zulfaalviandri', '$2y$10$wPeN/dPlH5BQa6VWwDd5geGzk5aDFlonRC3QY.FQJDwpYqxeaRbKi', '2022-07-03 09:17:23', '2022-07-03 09:17:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cekout`
--
ALTER TABLE `cekout`
  ADD PRIMARY KEY (`id_ck`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `cekout`
--
ALTER TABLE `cekout`
  MODIFY `id_ck` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
