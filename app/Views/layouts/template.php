<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Toko ABC JUICE</title>
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
    <header id="index">
        <div class="navbar">
            <h2>ABC JUICE</h2>
            <div class="nav_links">
                <ul>
                    <li><a href="/pages/index">Beranda</a></li>
                    <li><a href="/barang/index">Menu</a></li>
                    <li><a href="/pages/register">Daftar Akun</a></li>
                    <li><a href="/cekout"><img src="/img/logokrj.png" alt=""></a></li>
                </ul>
            </div>
            <a href="/auth/logout"><img width="30px" height="30px" src="/img/logout12.png" alt=""></a>
        </div>
        <h1> Jus egar yang mengandung vitaman untuk tubuh kita</h1>
        <p> Duduk dirumah saja langsung klik keranjang pada jus yang Anda pilih</p>
    </header>
    <?= $this->rendersection('content'); ?>
    <footer>
        <div class="fot">
            <div class="title">
                <h3>JUICE ABC</h3>
                <br><br>
                <p>Keuntungan minum jus buah di kami  <br>
                    adalah memberikan dengan buah berkualitas di peting langsung di kebun kita <br>
                    dengan penanaman yang tidak sembarang jadi vitaman di dalam buat terjaga.</p>
    
            </div>
            <div class="foot">
                <h4>Follow Sosial Media Kami!!!</h4>
                <ul>
                    <li><a href="https://www.facebook.com/">Facebook</a></li>
                    <li><a href="https://www.instagram.com/">Instagram</a></li>
                </ul>
                
            </div>
        </div>
        <div class="bot">
            <div class="copy">
                <ul>
                    <li><a href="">Terms & Conditions</a></li>
                    <li><a href="">Privacy Policy</a></li>
                </ul>
            </div>
        </div>
    </footer>
    
    
</body>
</html>