<?= $this->extend('layouts/template'); ?>
<?= $this->section('content'); ?>

<div class="cont-card">
    <?php foreach ($barang as $m) : ?>
        <div class="card">
            <div class="barang">
                <img src="/img/<?= $m['gambar']; ?>" alt="">
            </div>
            <div class="detail">
                <h3 name="nama" value="<?= $m['nama']; ?>"><?= $m['nama']; ?></h3>
                <div class="harga">
                    <p name="harga"><?= $m['harga']; ?></p>
                    <a href="/cekout/<?= $m['id']; ?>/create"><img src="/img/tambah.png" alt=""></a>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
        
    </div>

<?= $this->endsection(); ?>ction(); ?>