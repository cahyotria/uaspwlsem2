<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="">
    <title>Signin Template · Bootstrap v5.0</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >



    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="/docs/5.0/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="/docs/5.0/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="/docs/5.0/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="manifest" href="/docs/5.0/assets/img/favicons/manifest.json">
    <link rel="mask-icon" href="/docs/5.0/assets/img/favicons/safari-pinned-tab.svg" color="#7952b3">
    <link rel="icon" href="/docs/5.0/assets/img/favicons/favicon.ico">
    <meta name="theme-color" content="#7952b3">


    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
</head>

    <body>
        <div class="container">
          <?php if (!empty(session()->getFlashdata('error'))) : ?>
              <div class="alert alert-warning alert-dismissible fade show" role="alert">
                  <?php echo session()->getFlashdata('error'); ?>
              </div>
          <?php endif; ?>
          <form action="/auth/login" method="POST">
              <h1>Login</h1>
              <div class="form-group">
              <input type="text" name="username" id="username" placeholder="Username" class="form-control" required autofocus>
              </div>
              <div class="form-group">
              <input type="password" name="password" id="password" placeholder="Password" class="form-control" required>
              </div>
              <div class="form-group">
                      <button type="submit" class="btn btn-primary">Login</button>
              </div>

              <div class="buttonHolder"> 
                    <p>Belum Punya Akun? <a href="/pages/register">Daftar</a></p>
                </div>
          </form>
        </div>  



</body>

 </html>