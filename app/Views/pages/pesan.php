<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.88.1">
    <title>Checkout example · Bootstrap v5.1</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/checkout/">

    

    <!-- Bootstrap core CSS -->
<link href="/assets/css/bootstrap.min.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    
    <!-- Custom styles for this template -->
    <link href="form-validation.css" rel="stylesheet">
  </head>
  <body class="bg-light">
    
<div class="container">
  <main>
    <div class="py-5 text-center">
      <h2>Checkout form</h2>
      <p class="lead">CHECK KEMBALI PESANAN ANDA.</p>
    </div>

      
      <div class="col-md-7 col-lg-8">
        <h4 class="mb-3">ISI DENGAN BENAR</h4>
        <form class="needs-validation" novalidate>
          <div class="row g-3">
            <div class="col-sm-6">
              <label for="nama" class="form-label">Nama Lengkap</label>
              <input type="text" class="form-control" id="nama" placeholder="" value="" required>
              <div class="invalid-feedback">
                Valid first name is required.
              </div>
            </div>

            <div class="col-12">
              <label for="username" class="form-label">Username Akun</label>
              <div class="input-group has-validation">
                <input type="text" class="form-control" id="username" placeholder="Username Akun" required>
              <div class="invalid-feedback">
                  Your username is required.
                </div>
              </div>
            </div>

            <div class="col-12">
              <label for="number" class="form-label"> Jumlah Order </label>
              <input type="number" class="form-control" id="jumlah" placeholder="Tulisa jumlah pesanan">
            </div>

            <div class="col-12">
              <label for="address" class="form-label">Alamat</label>
              <input type="text" class="form-control" id="address" placeholder="Jl. ****" required>
              <div class="invalid-feedback">
                Please enter your shipping address.
              </div>
            </div>

            
            <div class="form-check">
              <input id="credit" name="kota" type="radio" class="form-check-input" checked required>
              <label class="form-check-label" for="credit">Kota Semarang</label>
            </div>
            

            <div class="col-md-4">
              <label for="state" class="form-label">Kecamatan</label>
              <select class="form-select" id="state" required>
              <option value="">Pilih Kecamatan...</option>
                <option>TEMBALANG</option>
                <option>GENUK</option>
                <option>PEDURUNGAN</option>
                <option>BANYUMANIK</option>
                <option>SAMPANGAN</option>
              </select>
            </div>
          </div>

          <hr class="my-4">

          <h4 class="mb-3">Pembayaran</h4>

          <div class="my-3">
            <div class="form-check">
              <input id="dana" name="payment" type="radio" class="form-check-input" checked required>
              <label class="form-check-label" for="credit">Dana</label>
            </div>
            <div class="form-check">
              <input id="debit" name="payment" type="radio" class="form-check-input" required>
              <label class="form-check-label" for="debit">Debit card</label>
            </div>
            <div class="form-check">
              <input id="gopay" name="payment" type="radio" class="form-check-input" required>
              <label class="form-check-label" for="paypal">GoPay</label>
            </div>
          </div>

          <hr class="my-4">

          <button class="w-100 btn btn-primary btn-lg" type="submit">Continue to checkout</button>
        </form>
      </div>
    </div>
  </main>

  <footer class="my-5 pt-5 text-muted text-center text-small">
    <p class="mb-1">&copy; 2022 ABC JUICE</p>
  </footer>
</div>

    <script src="/assets/js/bootstrap.bundle.min.js"></script>

  </body>
</html>
