<?= $this->extend('layouts/template'); ?>
<?= $this->section('content'); ?>

<!DOCTYPE html>
<html>
<head>
<style>
table, th, td {
  border: 1px solid;
}

table {
  width: 100%;
}
</style>
</head>
<body>

<h1 style="text-align:center;">Keranjang Belanja</h1>

<table> 
    

  <tr>
    <th>Gambar</th>
    <th>Nama Barang</th>
    <th>Harga</th>
    <th>Aksi</th>
  </tr>
  
  <?php
$total = 0;
foreach ($cekout as $b): ?>
  <tr>
    <td><img src="/img/<?= $b['gambar_ck']; ?>" alt=""></td>
    <td> <?= $b['nama_ck']; ?></td>
    <td>Rp. <?= $b['harga_ck']; ?></td>
    <?php $total += $b['harga_ck']; ?>
    <td><a href="/cekout/<?= $b['id_ck']; ?>/delete">Hapus</a></td>
  </tr>
  <?php
endforeach; ?>
  <tr>
    <td colspan="2">Total Harga</td>
    <td colspan="2">Rp. <?= $total; ?></td>
  </tr>
</table>
<a href="/pages/pesan"><h3 style="text-align:right;">Checkout</h3></a>
</body>

</html>


<?= $this->endsection(); ?>