<?= $this->extend('layouts/template'); ?>
<?= $this->section('content'); ?>

<div class="container">
        <div class="cont-top">
            <div class="isi1">
                <h2>Keuntungan Dari Kami</h2>
            </div>
            <div class="isi">
                <h3>Buah Segar</h3>
                <p>Keuntungan buah segar yaitu kaya Sumber Air dan Gizi, sumber air untuk tubuh dan kebutuhan gizi yang dapat meningkatkan metabolisme tubuh.</p>
            </div>
            <div class="isi">
                <h3>Harga Pelajar</h3>
                <p>Anda bisa mendapatkan mendapatkan buah segar dan berkualitas dengan harga murah.</p>
            </div>
            <div class="isi">
                <h3>Banyak Pilihan Buah</h3>
                <p>Kami menyediakan jenis buah sangat bermanfaat bagi tubuh kita tentunya dengan musim panas saat ini cocok buat menyegarkan pada siang hari.</p>
            </div>
        </div>
</div>
<?= $this->endsection(); ?>