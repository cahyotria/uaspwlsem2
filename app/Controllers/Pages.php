<?php

namespace App\Controllers;

class Pages extends BaseController
{
    public function index()
    {
        return view('pages/index');
        
    }
    public function galeri()
    {
        return view('pages/galeri');
        
    }
    public function regist()
    {
        return view('pages/regist');
        
    }
    public function table()
    {
        return view('pages/table');
        
    }
    public function login()
    {
        return view('pages/login');
    }
    public function register()
    {
        return view('pages/regist');  
    }

    public function pesan()
    {
        echo view('pages/pesan');  
    }

}
