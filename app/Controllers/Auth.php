<?php

namespace App\Controllers;

use  \App\Models\UsersModel;

class Auth extends BaseController
{
    public function index()
    {
        return view('pages/register');
    }
    public function register()
    {
        $users = new UsersModel();
        $users->insert([ 
            'username' => $this->request->getVar('username'),
            'password' => password_hash($this->request->getVar('password'), PASSWORD_BCRYPT),
        ]);
        return view('pages/login');
    }
    public function login()
    {
        $users = new UsersModel();
        $username = $this->request->getVar('username'); 
        $password = $this->request->getVar('password');
        $dataUser = $users->where([  //pencarian data,, $users merupakan objek
            'username' => $username,
        ])->first();
        if ($dataUser) {
            if (password_verify($password, $dataUser->password)) {
                session()->set([
                    'username' => $dataUser->username,
                    'logged_in' => TRUE
                ]);
                $Barang = $this->BarangModel->findAll();
                $data = [
                    
                    'Barang' => $Barang
                ];
                return view('pages/index', $data);
            } else {
                session()->setFlashdata('error', 'Username & Password Salah');
                return redirect()->back();
            }
        } else {
            session()->setFlashdata('error', 'Username & Password Salah');
            return redirect()->back();
        }
    }
    public function logout()
    {
        session()->destroy();
        return redirect()->to('/');
    }

}