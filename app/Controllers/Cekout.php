<?php

namespace App\Controllers;

use App\Models\CekoutModel;
use App\Models\BarangModel;

class Cekout extends BaseController
{
    protected $CekoutModel;
    public function __construct()
    {
        $this->CekoutModel = new CekoutModel();
    }
    public function index()
    {
        $cekout = $this->CekoutModel->findAll();
        $data = [
            'cekout' => $cekout
        ];
        return view('pages/table',$data);
    }
    public function create($id = null)
    {
        
        $barang = new BarangModel();
        $cekout = new CekoutModel();
        $data = $barang->getDataById($id);
        $query = [
            'gambar_ck' => $data['gambar'],
            'nama_ck' =>  $data['nama'],
            'harga_ck' => $data['harga']  
        ];
        $cekout->createData($query);
        return redirect('cekout');
    }
    public function delete($id = null)
    {
        $cekout = new CekoutModel();
        $cekout->delete($id);
        return redirect('cekout');
    }
}