<?php

namespace App\Controllers;

use \App\Models\BarangModel;

class Barang extends BaseController
{
    protected $BarangModel;
    public function __construct()
    {
        $this->BarangModel = new BarangModel();
    }

    public function index()
    {
        $barang = $this->BarangModel->findAll(); //Mengambil semua hasil,
        $data = [
            'barang' => $barang
        ];
        return view('pages/galeri',$data);
        
    }
    public function create()
    {
        $this->BarangModel->insert([
            'barang' => $this->request->getVar('barang')
        ]);
        return redirect()->back();
        
    }
    
   
}
