<?php

namespace App\Models;

use CodeIgniter\Model;

class BarangModel extends Model
{
    protected $table      = 'barang';
    protected $primaryKey = 'id';
    protected $useAutoIncrement = true;
    protected $allowedFields = ['nama', 'harga'];
    protected $returnType = 'array';

    public function getDataById($id)
    {
        return $this->where('id',$id)->first();
    }
}