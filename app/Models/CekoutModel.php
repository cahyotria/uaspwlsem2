<?php

namespace App\Models;

use CodeIgniter\Model;

class CekoutModel extends Model
{
    protected $table      = 'cekout';
    protected $primaryKey = 'id_ck';
    protected $useAutoIncrement = true;
    protected $allowedFields = ['nama_ck', 'harga_ck','gambar_ck'];
    protected $returnType = 'array';

    public function createData($data)
    {
        return $this->insert($data);
    }
    public function getDataById($id)
    {
        return $this->where('id_ck',$id)->first();
    }

}